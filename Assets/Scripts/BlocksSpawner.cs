﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class BlocksSpawner : MonoBehaviour
{
    private const float PixelNumber = 256f;
    private const float Speed = 4.0f;

    [SerializeField] int blocksHorizontal = 10;
    [SerializeField] int blocksVertikal = 10;

    [SerializeField] Block blockPrefab;

    private List<GameObject> rows = new List<GameObject>();

    private float blockWidth;
    private float blockHeight;
    private float changeOfPositionX;
    private float changeOfPositionY;
    private float endPosition;
    private float endPointY;

    private float emptyCell;
    private float emptyCellTwo;
    private float exception;
    private float randomNumber;

    private Vector2 bounds;

    public void StartField(Vector2 bounds)
    {
        this.bounds = bounds;
        float pixelWidth = Screen.width / blocksHorizontal;
        float pixelHeight = Screen.height / blocksVertikal;

        blockWidth = pixelWidth / PixelNumber;
        blockHeight = pixelHeight / PixelNumber;

        changeOfPositionX = bounds.x / blocksHorizontal;
        changeOfPositionY = bounds.y / blocksVertikal;

        endPosition = (-bounds.y * 4) - changeOfPositionY;

        endPointY = -bounds.y - changeOfPositionY;

        emptyCell = blocksHorizontal / 2;
        emptyCellTwo = emptyCell;
        exception = emptyCellTwo;

        for (int i = 0; i < blocksVertikal + 2; i++)
        {
            GameObject row = new GameObject("Row");
            row.transform.SetParent(transform);
            row.transform.position = new Vector2(0, -bounds.y + changeOfPositionY + changeOfPositionY * 2 * i);
            RowProperty(row);
            CreateStartBlocks(row);
        }

        GameManager.Instance.Person.SetParameters(blocksHorizontal, changeOfPositionX, blockWidth);
    }

    private void RowProperty(GameObject row)
    {
        float distance = Vector2.Distance(new Vector2(0, endPosition), row.transform.position);
        float duration = distance / Speed;

        row.transform.DOMoveY(endPosition, duration);
        rows.Add(row);
    }

    private void CreateStartBlocks(GameObject row)
    {
        for (int i = 0; i < blocksHorizontal; i++)
        {
            if (emptyCell != i)
            {
                CreateBlocks(row, i);
            }
        }
    }

    private void SpawnerRowUpper()
    {
        Destroy(rows[0]);
        rows.RemoveAt(0);

        GameObject row = new GameObject("Row");
        row.transform.SetParent(transform);
        float rowPosition = rows[rows.Count - 1].transform.position.y;
        row.transform.position = new Vector2(0, rowPosition + changeOfPositionY * 2);
        RowProperty(row);
        CreateMaze(row);
    }

    private void CreateMaze(GameObject row)
    {
        GenerationEmptyCell();

        for (int i = 0; i < blocksHorizontal; i++)
        {
            if (emptyCell != i && emptyCellTwo != i)
            {
                CreateBlocks(row, i);
            }
        }

        exception = emptyCellTwo;
        emptyCellTwo = emptyCell; 
    }

    private void GenerationEmptyCell()
    {
        randomNumber = Random.value;
        if (randomNumber < 0.3f)
        {
            emptyCell--;
        }
        else if (randomNumber >= 0.3f && randomNumber <= 0.6f)
        {
            emptyCell++;
        }

        if (emptyCell == 0)
        {
            emptyCell++;
        }
        else if (emptyCell == blocksHorizontal - 1)
        {
            emptyCell--;
        }

        if (emptyCell == exception)
        {
            GenerationEmptyCell();
            emptyCell = emptyCellTwo;
        }
    }


    private void CreateBlocks(GameObject row, int i)
    {
        Block block = Instantiate(blockPrefab, row.transform);
        block.transform.localPosition = new Vector2(-bounds.x + changeOfPositionX + changeOfPositionX * 2 * i, 0);
        block.transform.localScale = new Vector2(blockWidth, blockHeight);
    }

    public void Move(bool isSwitch)
    {
        for (int i = 0; i < rows.Count; i++)
        {
            GameObject row = rows[i];

            if (isSwitch)
                row.transform.DOPlay();
            else
                row.transform.DOPause();
        }
    }

    public void DestroyMaze()
    {
        for (int i = 0; i < rows.Count; i++)
        {
            Destroy(rows[i]);
        }
        rows.Clear();
    }

    private void Update()
    {
        if (rows.Count != 0)
        {
            if (rows[0].transform.position.y <= endPointY)
            {
                SpawnerRowUpper();
            }
        }
    }
}
