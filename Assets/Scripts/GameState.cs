﻿public enum GameState
{
    None = 0,
    Start = 1,
    Play = 2,
    Pause = 3,
    Losing = 4
}
