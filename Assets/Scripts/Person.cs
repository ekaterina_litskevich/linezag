﻿using UnityEngine;
using System;

public class Person : MonoBehaviour
{
    public event Action OnCollision;

    private Vector2 startPosition = Vector2.zero;
    private Vector2 endPosition;
    private Vector2 distance;

    private bool control = true;
    private bool mouseDown;

    public void SetParameters(float blocksHorizontal, float changeOfPositionX, float blockWight)
    {
        if (blocksHorizontal % 2 != 0)
        {
            transform.position = new Vector2(0, transform.position.y);
        }
        else
        {
            transform.position = new Vector2(0 + changeOfPositionX, transform.position.y);
        }

        transform.localScale = Vector3.one * blockWight * 5;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        OnCollision?.Invoke();
    }

    public void InitialPosition()
    {
        distance = Vector2.zero;
        startPosition = Vector2.zero;
        endPosition = Vector2.zero;
    }

    public void MotionControl(bool control)
    {
        this.control = control;
    }

    void Update()
    {
        if (control)
        {
            if (Input.GetMouseButtonDown(0))
            {
                mouseDown = true;
            }

            if (Input.GetMouseButtonUp(0))
            {
                mouseDown = false;
                InitialPosition();
            }

            if (mouseDown)
            {
                transform.position = new Vector2(transform.position.x + distance.x, transform.position.y);

                if (startPosition != Vector2.zero)
                {
                    endPosition = Input.mousePosition;
                    endPosition = Camera.main.ScreenToWorldPoint(endPosition);
                }

                distance = endPosition - startPosition;

                startPosition = Input.mousePosition;
                startPosition = Camera.main.ScreenToWorldPoint(startPosition);
            }
        }
    }
}
