﻿using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] Button pauseButton;
    [SerializeField] Button continueButton;
    [SerializeField] Button restartButton;
    [SerializeField] Image scoreImage;
    [SerializeField] Image recordImage;
    [SerializeField] Image scoreEndImage;
    [SerializeField] Text scoreText;
    [SerializeField] Text recordText;
    [SerializeField] Text scoreEndText;

    [SerializeField] Canvas mainCanvas;

    [SerializeField] BlocksSpawner blocksSpawner;
    [SerializeField] Person personPrefab;
    [SerializeField] Camera cam;

    private Person person;

    public Person Person
    {
        get
        {
            return person;
        }
    }

    private Vector2 bounds;

    private float record;
    private float score;

    private GameState gameState;

    public static GameManager Instance;

    private void Awake()
    {
        DontDestroyOnLoad(this);
        Instance = this;
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    void Start()
    {
        gameState = GameState.Start;

        cam.orthographicSize = Screen.height / 200;
        bounds = cam.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));

        Instantiate(Resources.Load(gameState.ToString() + "UI"), mainCanvas.transform);

        pauseButton.onClick.AddListener(PauseButton_OnClick);
        continueButton.onClick.AddListener(ContinueButton_OnClick);
        restartButton.onClick.AddListener(RestartButton_OnClick);
    }

    public void StartButton_OnClick()
    {
        gameState = GameState.Play;

        scoreEndImage.gameObject.SetActive(false);
        scoreImage.gameObject.SetActive(true);
        pauseButton.gameObject.SetActive(true);

         person = Instantiate(personPrefab, transform);

        person.OnCollision += Person_OnCollision;

        blocksSpawner.StartField(bounds);

       
    }

    private void PauseButton_OnClick()
    {
        gameState = GameState.Pause;

        person.MotionControl(false);
        blocksSpawner.Move(false);

        restartButton.gameObject.SetActive(true);
        continueButton.gameObject.SetActive(true);
    }

    private void RestartButton_OnClick()
    {
        restartButton.gameObject.SetActive(false);
        continueButton.gameObject.SetActive(false);
        recordImage.gameObject.SetActive(false);
        restartButton.gameObject.SetActive(false);

        Destroy(person.gameObject);
        blocksSpawner.DestroyMaze();

        score = 0;

        StartButton_OnClick();
    }

    private void ContinueButton_OnClick()
    {
        gameState = GameState.Play;

        restartButton.gameObject.SetActive(false);
        continueButton.gameObject.SetActive(false);

        person.MotionControl(true);
        blocksSpawner.Move(true);
    }

    private void Person_OnCollision()
    {
        Losing();
    }

    private void Losing()
    {
        gameState = GameState.Losing;

        blocksSpawner.Move(false);
        person.MotionControl(false);

        pauseButton.gameObject.SetActive(false);
        restartButton.gameObject.SetActive(true);
        scoreImage.gameObject.SetActive(false);
        recordImage.gameObject.SetActive(true);
        scoreEndImage.gameObject.SetActive(true);

        record = PlayerPrefs.GetFloat("record");

        if (score > record)
        {
            record = score;
            PlayerPrefs.SetFloat("record", record);
            PlayerPrefs.Save();
        }

        recordText.text = string.Format("Record: {0}", (int)record);
        scoreEndText.text = string.Format("Score: {0}", (int)score);
    }

    private void OnDisable()
    {
        pauseButton.onClick.RemoveListener(PauseButton_OnClick);
        continueButton.onClick.RemoveListener(ContinueButton_OnClick);
        restartButton.onClick.RemoveListener(RestartButton_OnClick);
    }

    private void Update()
    {
        if (gameState == GameState.Play)
        {
            score += Time.deltaTime;
            scoreText.text = string.Format("Score: {0}", (int)score);
        }
    }
}


