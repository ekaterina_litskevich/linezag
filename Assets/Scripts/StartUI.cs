﻿using UnityEngine;
using UnityEngine.UI;

public class StartUI : MonoBehaviour
{
    [SerializeField] Button startButton;


    private void OnEnable()
    {
        startButton.onClick.AddListener(StartButton_OnClick);
    }

    private void StartButton_OnClick()
    {
        GameManager.Instance.StartButton_OnClick();
        Destroy(gameObject);
    }

    private void OnDisable()
    {
        startButton.onClick.RemoveListener(StartButton_OnClick);
    }
}
